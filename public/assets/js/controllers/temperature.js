
function check(){
    var data = {
        temperature: jQuery('#temperature').val(),
        conversion_type: jQuery('#conversion_type option:selected').val()
    };

    if(!data.temperature || !data.conversion_type) {
        alert('To make the conversion is necessary to complete all fields');
    } else {
        sendToCheck(
            data,
            function(response) {
                if(response.status == 'error'){
                    alert('Error: '+response.message);
                } else{
                    jQuery('#result').val(response.resposta_temperatura);
                }
            }
        );
    }
}

function sendToCheck(data, callback) {
    jQuery.ajax({
        type: "POST",
        url: base_url + "/temperature/check",
        data: data,
        async: true,
        cache: true,
        beforeSend: function() {},
        success: function(response) {
            callback(response);
        },
        error: function(jqXHR, textStatus) {
            var response = {
                status: 'error',
                message: "There was an error sending ajax request"
            };
            callback(response);
        }
    });
}


