<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 18/04/15
 * Time: 23:24
 */

namespace Application\Models\Entity;

use Application\Services\Temperature\TemperatureService;

class Search extends  EntityInterface
{
    const CELSIUS_TO_FAHRENHEIT = 'C';
    const FAHRENHEIT_TO_CELSIUS = 'F';

    protected $id;
    protected $temperature;
    protected $conversionType;
    protected $temperatureConverted;
    protected $dateTime;
    protected $ip;
    protected $xml;

    public function __construct()
    {
        $map = array(
            'id'                    => 'id_consulta',
            'temperature'           => 'requisicao_temperatura',
            'conversionType'        => 'requisicao_tipo',
            'temperatureConverted'  => 'resposta_temperatura',
            'dateTime'              => 'datahora_consulta',
            'ip'                    => 'ip_requisitante',
            'xml'                   => 'xml_gerado',
        );
        parent::__construct($map);

        $this->setIp(getClientIp());
        $dateTime = new \DateTime();
        $this->setDateTime($dateTime->format('Y-m-d H:i:s'));
    }


    /**
     * @return mixed
     */
    public function getConversionType()
    {
        return $this->conversionType;
    }

    /**
     * @param mixed $conversionType
     * @return $this
     */
    public function setConversionType($conversionType)
    {
        $this->conversionType = $conversionType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @param mixed $dateTime
     * @return $this
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return number_format($this->temperature, 1, '.', '');
    }

    /**
     * @param mixed $temperature
     * @return $this
     */
    public function setTemperature($temperature)
    {
        $this->temperature = number_format($temperature, 1, '.', '');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemperatureConverted()
    {
        return number_format($this->temperatureConverted, 1, '.', '');
    }

    /**
     * @param mixed $temperatureConverted
     * @return $this
     */
    public function setTemperatureConverted($temperatureConverted)
    {
        $this->temperatureConverted = number_format($temperatureConverted, 1, '.', '');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param mixed $xml
     * @return $this
     */
    public function setXml($xml)
    {
        $this->xml = $xml;
        return $this;
    }



    ########################################################################################
    ### Logic business
    ########################################################################################

    /**
     *  Handles necessary elements to make the search
     */
    public function execute()
    {
        $service = new TemperatureService();
        $response = false;
        if($this->conversionType == self::CELSIUS_TO_FAHRENHEIT){

            $response = $service->convertCelsiusToFahrenheit($this->temperature);
            $this->setTemperatureConverted($response[$service::FAHRENHEIT]);

        } elseif($this->conversionType == self::FAHRENHEIT_TO_CELSIUS){

            $response = $service->convertFahrenheitToCelsius($this->temperature);
            $this->setTemperatureConverted($response[$service::CELSIUS]);

        } else{
            throw new \InvalidArgumentException('Invalid conversion type');
        }

        $this->setXml($response['xml']);

        return $this;
    }


}