<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 19/04/15
 * Time: 00:08
 */

namespace Application\Models\Entity;


abstract class EntityInterface
{
    /**
     * Array where the key is the class properties
     * and their values represent their corresponding fields in the database
     *
     * example
     * array(
     *     'id' => 'id_search',     //'id' is an property of class and 'id_search' is a column of some table
     *     'ip' => 'ip_client',
     *     'date' => 'date_time',
     * )
     *
     * @var array $map
     */
    protected $map;


    public function __construct(array $map)
    {
        if(empty($map)) throw new \InvalidArgumentException("Array can not be empty ");
        $this->map = $map;
    }


    public function toArray()
    {
        $reflector = new \ReflectionClass($this);
        $properties = $reflector->getProperties();

        $return = array();
        foreach($properties as $property) {
            $return[$property->getName()] = $this->{$property->getName()};
        }

        return $return;
    }



    public function toArrayDbFormat()
    {
        $array = $this->toArray();

        $return = array();
        foreach ($this->map as $property => $column) {
            if(array_key_exists($property, $array)){
                $return["{$column}"] = $this->{$property};
            }
        }
        return $return;
    }


}