<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Application\Models\Entity\Search;

class Search_model extends CI_Model
{
	
	function __construct(){
		parent::__construct();
	}
	
	
	function findAll(){
		// ->result(); Return all rows
		// ->row(); Return single row
        return $this->db->get('pesquisa')->result();
    }


    function findById($id){
        $this->db->select('pesquisa.*');
        $this->db->from('pesquisa');
        $this->db->where('id_pesquisa', $id);

        return $this->db->get('pesquisa')->row();
    }


	function insert(Search $search){
        $id = $this->db->insert('pesquisa', $search->toArrayDbFormat());
        return $search->setId($id);
	}


    function update(Search $search){
		$this->db->where('id_pesquisa', $search->getId());
		return $this->db->update('pesquisa', $search->toArrayDbFormat());
	}

}