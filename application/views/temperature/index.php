<h4>Temperature</h4>
<hr>

<div class="well">
    <p><span class="text-info">Enter with temperature and choose a conversion method.</span></p>
    <hr>
    <form>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="temperature">Temperature</label>
                <input id="temperature" type="number" step="0.1" class="form-control" name="temperature" required="required">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="conversion_type">Conversion type</label>
                <select id="conversion_type" class="form-control" name="conversion_type" required="required">
                    <option value="" selected="selected"></option>
                    <option value="C">Celsius to Fahrenheit</option>
                    <option value="F">Fahrenheit to Celsius</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label>Result</label>
                <input id="result" type="text" class="form-control" disabled="disabled" >
            </div>
        </div>
        <hr>
        <div class="row-fluid">
            <button type="button" id="check-temperature" onclick="javascript:check()" class="btn btn-info btn-block"><i class="fa fa-globe"></i> Check Now </button>
        </div>
    </form>
</div>


<!-- Script temperature.js -->
<script src="<?php echo base_url('public/assets/js/controllers/temperature.js')?>" type="text/javascript"></script>