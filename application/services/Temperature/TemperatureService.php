<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 18/04/15
 * Time: 23:58
 */

namespace Application\Services\Temperature;


class TemperatureService
{
    const FAHRENHEIT = 'fahrenheit';
    const CELSIUS = 'celsius';

    static $resource = "http://www.w3schools.com/webservices/tempconvert.asmx";

    protected $client;
    protected $functions;

    public function __construct()
    {
        $this->client = new \SoapClient(self::$resource."?WSDL", array(
            'trace' => true,
            'soap_version'   => SOAP_1_2,
            'encoding'=>'utf-8'
        ));

        $this->functions = $this->client->__getFunctions();

    }


    function convertCelsiusToFahrenheit($celsius)
    {
        if(!$celsius || !is_numeric($celsius)) throw new \InvalidArgumentException('Value passed as "celsius" not is valid');

        $response = $this->client->CelsiusToFahrenheit(array('Celsius' => $celsius));
        $xml = $this->client->__getLastRequest();

        return array(
            'xml' => $xml,
            'conversionType' => 'CelsiusToFahrenheit',
            self::CELSIUS => $celsius,
            self::FAHRENHEIT => $response->CelsiusToFahrenheitResult
        );
    }


    function convertFahrenheitToCelsius($fahrenheit)
    {
        if(!$fahrenheit || !is_numeric($fahrenheit)) throw new \InvalidArgumentException('Value passed as "celsius" not is valid');

        $response = $this->client->FahrenheitToCelsius(array('Fahrenheit' => $fahrenheit));
        $xml = $this->client->__getLastRequest();

        return array(
            'xml' => $xml,
            'conversionType' => 'FahrenheitToCelsius',
            self::CELSIUS => $response->FahrenheitToCelsiusResult,
            self::FAHRENHEIT => $fahrenheit
        );
    }
} 