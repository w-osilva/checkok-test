<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Application\Models\Entity\Search;

class Temperature extends CI_Controller
{

	public function index()
	{
        $this->template->load("layout/base", "temperature/index", array());
	}

    public function check()
    {
        header('Content-type: application/json; charset=UTF-8');

        //validation form
        $this->form_validation->set_rules('conversion_type', 'Conversion type', 'xss_clean|required');
        $this->form_validation->set_rules('temperature', 'Temperature', 'xss_clea|required');

        if($this->form_validation->run()) {

            //execute call to webservice
            try{
                $search = new Search();
                $search->setConversionType($this->input->post('conversion_type'))
                    ->setTemperature($this->input->post('temperature'))
                    ->execute();
            } catch(\Exception $e) {
                die("Exception: {$e->getMessage()}");
            }

            $this->load->model('search_model');

            if($this->search_model->insert($search)) {
                echo json_encode($search->toArrayDbFormat(), true);
                exit;
            }
        }

        echo json_encode(array('status'=>'error', 'message'=>'Input fields are invalid'), true
        );
        exit;
    }


}

