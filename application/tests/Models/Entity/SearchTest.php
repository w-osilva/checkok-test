<?php
/**
 * Created by PhpStorm.
 * User: washington
 * Date: 19/04/15
 * Time: 04:37
 */

namespace Application\Tests\Models\Entity;


use Application\Models\Entity\Search;

class SearchTest extends \PHPUnit_Framework_TestCase
{
    private $CI;

    /** @var  Search $search*/
    protected $search;

    protected function setUp()
    {
        $CI =& get_instance();
        $CI->load->helper('ip');

        $this->search = new Search();
    }

//    protected function tearDown()
//    {}
//
//    public static function setUpBeforeClass()
//    {}
//
//    public static function tearDownAfterClass()
//    {}

    public function testConvertCelsiusToFahrenheit()
    {
        $this->search->setTemperature('35.5')
            ->setConversionType(Search::CELSIUS_TO_FAHRENHEIT);

        $result = $this->search->execute();

        $this->assertInstanceOf('Application\Models\Entity\Search', $result);
        $this->assertEquals('95.9', $result->getTemperatureConverted());
    }
} 