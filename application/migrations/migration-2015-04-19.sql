SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `checkok`
--
CREATE DATABASE IF NOT EXISTS `checkok` CHARACTER SET utf8 COLLATE utf8_unicode_ci;
-- --------------------------------------------------------

--
-- Estrutura da tabela `pesquisa`
--

CREATE TABLE IF NOT EXISTS `pesquisa` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `requisicao_temperatura` float(3,1) NOT NULL,
  `requisicao_tipo` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'C- Celsius / F- Fahrenheit',
  `resposta_temperatura` int(11) NOT NULL,
  `datahora_consulta` datetime NOT NULL,
  `ip_requisitante` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `xml_gerado` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_consulta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
ALTER TABLE  `pesquisa` CHANGE  `resposta_temperatura`  `resposta_temperatura` FLOAT( 4, 1 ) NOT NULL ;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
