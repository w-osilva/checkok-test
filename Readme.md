# Application test

###Descrição

Construir uma tela que informe a temperatura e o tipo (C = Celsius / F = Fahrenheit).
 
Chamar o webservice: http://www.w3schools.com/webservices/tempconvert.asmx e de acordo com o tipo o método correspondente.

Gravar as pesquisas em uma tabela no banco de dados Mysql, na tabela "pesquisa", com os campos (id_consulta [gerar automatico], requisicao_temperatura, requisicao_tipo, resposta_temperatura, datahora_consulta, ip_requisitante, xml_gerado).



--
# Configurando o Projeto
#####Download
No seu diretorio de projetos web faça o clone do projeto:
```bash
$ cd /var/www/html
$ git clone git@bitbucket.org:w-osilva/checkok-test.git
```

#####Dependências (Composer)
Para adicionar/atualizar as dependencias do projeto, será necessário obter o arquivo composer.phar:
```bash
$ cd /var/www/html/checkok-test
$ curl -sS https://getcomposer.org/installer | php
```

Após o término do download, basta então pedir ao composer para baixar as dependencias que estão configuradas no arquivo composer.json 

```bash
$ php composer.phar install
```

#####Servidor PHP
No diretorio do projeto e execute o comando para rodar o servidor embutido do PHP:
```bash
$ cd /var/www/html/checkok-test
$ php -S localhost:8000
```

#####Mysql
Para criar as tabelas, execute os seguintes comandos substituindo os dados de usuario pelos seus:
```bash
$ cd /var/www/html/checkok-test/application/migrations
$ mysql -u root -p root -l 127.0.0.1 checkok < sql/migration-2015-04-19.sql
```

**OBS:** Não esquecer de configurar os dados do Banco no arquivo de configuração de databases do Codeigniter... 

Agora tente acesar o projeto http://localhost:8000

